export function isLoading() {
    return { type: "@common/IS_LOADING" };
}

export function isNotLoading() {
    return { type: "@common/IS_NOT_LOADING" };
}