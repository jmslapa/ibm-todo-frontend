import produce from 'immer';

const INITIAL_STATE = {
    loading: false
}

export default function common(state = INITIAL_STATE, {type, payload}) {
    return produce(state, draft => {
        switch (type) {
            case "@common/IS_LOADING":
                draft.loading = true
                break;

            case "@common/IS_NOT_LOADING":
                draft.loading = false
                break;

            default:
                draft = INITIAL_STATE;
                break;
        }
    });
}