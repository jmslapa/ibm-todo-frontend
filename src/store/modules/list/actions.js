import {store} from "../../index";

export function updateFilters(filters) {
    return {
        type: "@list/UPDATE_FILTERS",
        payload: filters
    };
}

export function fetchList() {
    return {
        type: "@list/FETCH",
        payload: store.getState().list.filters
    };
}

export function listFetched(page) {
    return {
        type: "@list/FETCHED",
        payload: page
    };
}

export function createTask(task) {
    return {
        type: "@list/CREATE",
        payload: task
    }
}

export function updateTask(task) {
    return {
        type: "@list/UPDATE",
        payload: task
    }
}

export function deleteTask(task) {
    return {
        type: "@list/DELETE",
        payload: task
    }
}

export function toggleCheckTask(task) {
    return {
        type: "@list/TOGGLE_CHECK",
        payload: task
    }
}