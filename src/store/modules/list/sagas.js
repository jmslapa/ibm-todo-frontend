import api from "../../../services/api";
import {fetchList, listFetched} from "./actions";
import {put, takeEvery} from 'redux-saga/effects'
import {toast} from "react-toastify";
import {isLoading, isNotLoading} from "../common/actions";

export function* fetchTodoList({payload}) {
    let page;
    yield api.post('todos/list/all', payload)
        .then(resp => {
            page = resp.data;
        })
        .catch(error => {
            toast.error('Error');
            console.log(error);
        });

    yield put(listFetched(page));
}

export function* createTask({payload}) {
    yield put(isLoading());
    yield api.post(`todos`, payload)
        .then(() => toast.success('Task created with success!'))
        .catch(error => {
            toast.error('Error');
            console.log(error);
        });
    yield put(isNotLoading());
    yield put(fetchList());
}

export function* updateTask({payload}) {
    yield put(isLoading());
    yield api.put(`todos/${payload.id}`, payload)
        .then(() => toast.success('Task updated with success!'))
        .catch(error => {
            toast.error('Error');
            console.log(error);
        });
    yield put(isNotLoading());
}

export function* deleteTask({payload}) {
    yield put(isLoading());
    yield api.delete(`todos/${payload.id}`)
        .then(() => toast.success('Task deleted with success!'))
        .catch(error => {
            toast.error('Error');
            console.log(error);
        });
    yield put(isNotLoading());
    yield put(fetchList());
}

export function* toggleCheckTask({payload}) {
    yield put(isLoading());
    let task = {...payload, complete: !payload.complete};
    yield api.put(`todos/${task.id}`, task)
        .then(() => toast.success('Task updated with success!!'))
        .catch(error => {
            toast.error('Error');
            console.log(error);
        });
    yield put(isNotLoading());
    yield put(fetchList());
}


export const listSagas = [
    takeEvery('@list/FETCH', fetchTodoList),
    takeEvery('@list/CREATE', createTask),
    takeEvery('@list/UPDATE', updateTask),
    takeEvery('@list/DELETE', deleteTask),
    takeEvery('@list/TOGGLE_CHECK', toggleCheckTask)
]