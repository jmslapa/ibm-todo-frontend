import produce from 'immer';
import moment from "moment";

const INITIAL_STATE = {
    page: {
        content: []
    },
    filters: {
        period: moment().format("YYYY-MM-DD")
    }
}

export default function list(state = INITIAL_STATE, {type, payload}) {
    return produce(state, draft => {
       switch (type) {
           case "@list/FETCHED":
               draft.page = {content: payload}
           break;

           case "@list/UPDATE_FILTERS":
               draft.filters = payload
               break;

           default:
               draft = INITIAL_STATE;
           break;
       }
    });
}