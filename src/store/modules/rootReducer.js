import {combineReducers} from 'redux';
import list from "./list/reducer";
import common from "./common/reducer";

export default combineReducers({
    list,
    common
});