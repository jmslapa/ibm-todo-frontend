import {persistStore} from 'redux-persist';
import createStore from './createStore';
import persistReducers from './persistReducers';
import rootReducer from './modules/rootReducer';
import createSagaMiddleware from 'redux-saga'
import rootSagas from "./modules/rootSagas";

const sagaMiddleware = createSagaMiddleware();

const middlewares = [
    sagaMiddleware
];

const store = createStore(persistReducers(rootReducer), middlewares);
const persistor = persistStore(store);

sagaMiddleware.run(rootSagas)

export {store, persistor};