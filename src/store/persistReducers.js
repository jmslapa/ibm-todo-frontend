import storage from 'redux-persist/lib/storage';
import {persistReducer} from 'redux-persist';

const persistReducers = (reducers) => {
    return persistReducer(
        {
            key: 'todolist',
            storage,
            whitelist: ['list'],
        },
        reducers
    );
};

export default persistReducers;