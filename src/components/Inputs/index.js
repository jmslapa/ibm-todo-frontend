import styled, {css} from "styled-components";

const FormButtons = css`
  all: unset;
  height: 38px;
  text-align: center;
  font-size: 18px;
  border-radius: 5px;
  padding: 0 20px;
  width: auto;
  cursor: pointer;
  transition: 0.2s, 0.1s transform;

  :active {
    transform: scale(0.95);
  }
`;

export const SubmitBtn = styled.button`
  ${FormButtons};
  color: mediumslateblue;
  :hover {
    color: #fff;
    background: mediumslateblue;
  }
`;

export const ResetBtn = styled.button`
  ${FormButtons};
  color: grey;
  :hover {
    color: #fff;
    background: grey;
  }
`;

export const InlineForm = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;

  input[type="submit"] {
    color: mediumslateblue;
    :hover {
      color: #fff;
      background: mediumslateblue;
    }
  }
`;

export const FormGroup = styled.div`
  display: flex;
  position: relative;
  flex-direction: ${props => props.column ? "column" : "row"};
  align-items: center;
  height: 40px;
  font-size: 16px;
  font-weight: 500;
  color: #555;
  
  label:first-child {
    display: inline-block;
    margin-right: 10px;
    font-weight: 600;
    width: 50px;
  }
  
  input {
    height: 38px;
    padding: 0 10px;
    border: 1px solid #e5e5e5;
    border-radius: 5px;
    font-size: 16px;
    flex-grow: ${props => props.block ? 1 : 0};
  }
  
  fieldset {
    position: relative;
    border: 1px solid #e5e5e5;
    
    legend {
      all:unset;
      padding: 5px;
      font-size: 16px;
      font-weight: 600;
    }
    
    textarea {
      border: none;
      resize: unset;
      padding: 10px;
      font-weight: 500;
      font: unset;
      width: 385px;
    }
  }
`;

export const RadioGroup = styled(FormGroup)`
  label:first-child {
    margin-right: 0;
  }
  
  > ${FormGroup} {
    input[type="radio"] {
      margin-left: 10px;
      margin-right: 10px;
      transform: scale(1.5);
      
      + label {
        margin-right: 15px;
      }
    }
  }
`;

