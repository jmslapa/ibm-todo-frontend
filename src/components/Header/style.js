import styled from "styled-components";
import { CardChecklist } from 'styled-icons/bootstrap'

export const Container = styled.div`
  display: flex;
  align-items: center;
  background: mediumslateblue;
  padding: 20px 30px 20px 10px;
  color: #fff;
  
  & * {
    margin-left: 20px;
  }
`;

export const Brand = styled(CardChecklist)`
    width: 40px;
`;

export const Title = styled.h1`
`;