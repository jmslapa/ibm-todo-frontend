import { Container, Brand, Title } from './style';

const Header = () => {
    return (
        <Container>
            <Brand/>
            <Title>{ "Todo List" }</Title>
        </Container>
    );
}

export default Header;