import styled from "styled-components";

export const NewTask = styled.button`
  svg {
    pointer-events: none;
    width: 50px;
    color: #fff;
  }
`;

export const Container = styled.div`
  position: relative;
  width: 480px;
  height: 720px;
  background: #fff;
  border-radius: 20px;
  box-shadow: 2px 2px 10px #ccc;
  overflow: hidden;
  
  ${NewTask} {
    position: absolute;
    top: 15px;
    right: 30px;
  }
`;