
import {Container, NewTask} from './style.js';
import {PlusCircle} from 'styled-icons/boxicons-regular'
import Header from "../Header";
import TodoList from "../List";
import FilterBoard from "../FilterBoard";
import {useState} from "react";
import CreationModal from "../CreationModal";

const Panel = (props) => {

    const [creating, setCreating] = useState(false);

    return (
        <Container>
            <NewTask onClick={() => setCreating(true)}>
                <PlusCircle/>
            </NewTask>
            {
                creating
                    ? <CreationModal onFinish={() => setCreating(false)}/>
                    : false
            }
            <Header/>
            <FilterBoard/>
            <TodoList />
        </Container>
    );
}

export default Panel;