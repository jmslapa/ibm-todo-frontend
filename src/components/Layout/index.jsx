import {Container} from "./style";
import Panel from "../Panel";

const Layout = (props) => {
    return (
        <>
            <Container>
                <Panel/>
            </Container>
        </>
    );
}

export default Layout;