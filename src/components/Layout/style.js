import styled from "styled-components";


export const Container = styled.div`
  height: 100vh;
  width: 100vw;
  background: #fefefe;
  display: flex;
  justify-content: center;
  align-items: center;
`;