import { Item } from './style'
import Actions from "../Actions";
import Task from "../Task";
import {useState} from "react";
import {useDispatch} from "react-redux";
import {deleteTask, toggleCheckTask } from "../../store/modules/list/actions";

const ListItem = ({ task }) => {

    const dispatch = useDispatch();
    const [editing, setEditing] = useState(false);

    function onDelete() {
        dispatch(deleteTask(task));
    }
    function onCheck() {
        dispatch(toggleCheckTask(task));
    }

    return (
        <Item>
            <Actions
                onEdit={() => setEditing(!editing)}
                onDelete={onDelete}
                onCheck={onCheck}
                checked={task.complete}
            />
            <Task
                task={task}
                editing={editing}
                setEditing={setEditing}
            />
        </Item>
    );
}

export default ListItem;