import {Container, List} from "./style";
import ListItem from "./ListItem";
import {useEffect, useState} from "react";
import {useSelector} from "react-redux";

const TodoList = (props) => {
    const state = useSelector(state => state.list.page);
    const [tasks, setTasks] = useState([]);

    useEffect(() => {
        setTasks(state.content.map(task => {
            return (
                <ListItem key={`list_${task.id}`} task={task} />
            );
        }))
    }, [state.content]);

    return (
        <Container>
            <List>
                {tasks}
            </List>
        </Container>
    );
}

export default TodoList;
