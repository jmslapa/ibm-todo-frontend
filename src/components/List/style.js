import styled from "styled-components";

export const Container = styled.div`
  max-height: 460px;
  overflow-x: hidden;
  overflow-y: scroll;
`;

export const List = styled.ul`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;
  margin: 15px 10px;
  padding: 0px 5px;
`;

export const Item = styled.li`
  position: relative;
  min-height: 40px;
  width: 100%;
  margin-bottom: 5px;
  border-radius: 5px;
  overflow: hidden;

  ::after {
    content: "";
    position: absolute;
    top: 0;
    left: 0;
    background: mediumslateblue;
    width: 10px;
    height: 100%;
    border-radius: 5px 0 0 5px;
  }
`;