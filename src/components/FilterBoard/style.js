import styled from "styled-components";

export const Container = styled.div`
  margin-top: 20px;
  padding: 0 25px 0 15px;
  > * {
    margin: 5px 0;
  }
`;