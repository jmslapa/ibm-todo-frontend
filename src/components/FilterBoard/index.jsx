import {Container} from "./style";
import {useDispatch, useSelector} from "react-redux";
import {fetchList, updateFilters} from "../../store/modules/list/actions";
import {useEffect, useState} from "react";
import {store} from "../../store";
import {FormGroup, InlineForm, RadioGroup, SubmitBtn} from "../Inputs";
import moment from "moment";

const FilterBoard = () => {

    const filters = useSelector(state => state.list.filters);
    const dispatch = useDispatch();
    const [title, setTitle] = useState(filters.title);

    function handleRadioChange(event) {
        let draft = {...filters};
        let value = event.target.value;
        if (value.length === 0) {
            delete draft.complete;
        } else {
            draft.complete = value === "true";
        }
        dispatch(updateFilters(draft));
    }

    function handleReset() {
        dispatch(updateFilters({period: moment().format("YYYY-MM-DD")}));
        setTitle("");
    }

    useEffect(() => {
        dispatch(fetchList())
    }, [filters, dispatch]);

    useEffect(() => {
        const filters = store.getState().list.filters;
        const timeout = setTimeout(() => dispatch(updateFilters({...filters, title})), 500);
        return () => clearTimeout(timeout);
    }, [title, dispatch]);

    return (
        <Container>
            <FormGroup block>
                <label htmlFor="title">Title</label>
                <input type="text" id="title" placeholder="Título" value={title}
                    onChange={(e) => setTitle(e.target.value)}/>
            </FormGroup>
            <RadioGroup>
                <label>Status</label>
                <FormGroup>
                    <input type="radio" name="status" id="any" value={undefined} checked={filters.complete === undefined}
                           onChange={handleRadioChange}/>
                    <label htmlFor="any">Qualquer</label>
                </FormGroup>
                <FormGroup>
                    <input type="radio" name="status" id="complete" value={true} checked={filters.complete === true}
                           onChange={handleRadioChange}/>
                    <label htmlFor="complete" >Finalizado</label>
                </FormGroup>
                <FormGroup>
                    <input type="radio" name="status" id="incomplete" value={false} checked={filters.complete === false}
                           onChange={handleRadioChange}/>
                    <label htmlFor="incomplete">Pendente</label>
                </FormGroup>
            </RadioGroup>
            <InlineForm>
                <FormGroup>
                    <label htmlFor="period">Period</label>
                    <input type="date" id="period" value={filters.period}
                           onChange={(e) => dispatch(updateFilters({...filters, period: e.target.value}))}/>
                </FormGroup>
                <SubmitBtn onClick={handleReset}>Limpar Filtros</SubmitBtn>
            </InlineForm>
        </Container>
    );
}

export default FilterBoard;