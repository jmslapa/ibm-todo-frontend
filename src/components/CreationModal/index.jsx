import {FormGroup, InlineForm, ResetBtn, SubmitBtn} from "../Inputs";
import {Container, Modal} from "./style";
import moment from "moment";
import * as yup from 'yup';
import {useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {createTask} from "../../store/modules/list/actions";
import {toast} from "react-toastify";

const today = moment().format("YYYY-MM-DD");
const initialFormData = {
    title: "",
    period: today,
    description: "",
    complete: false
}

const CreationModal = ({ onFinish }) => {
    const dispatch = useDispatch();
    const [form, setForm] = useState(initialFormData);
    const loading = useSelector(state => state.common.loading);

    const todoSchema = yup.object().shape({
        title: yup.string().min(3).max(100).required(),
        // period: yup.date().min(today).required(),
        description: yup.string().min(20).max(500),
        complete: yup.boolean().required()
    });

    function handleSubmit() {
        todoSchema.validate(form)
            .then(() => {
                dispatch(createTask(form));
            })
            .catch(err => toast.error(err.message.charAt(0).toUpperCase() + err.message.slice(1)))
            .finally(() => onFinish());
    }

    function handleReset() {
        setForm(() => initialFormData);
        onFinish();
    }

    return(
        <Container>
            <Modal>
                <FormGroup block>
                    <label htmlFor="creating_title">Title</label>
                    <input type="text" id="creating_title" placeholder={"Título"} value={form.title}
                           onChange={(e) => setForm(form => ({...form, title:e.target.value}))}/>
                </FormGroup>
                <FormGroup>
                    <label htmlFor="creating_period">Period</label>
                    <input type="date" id="creating_period" value={form.period}
                           onChange={(e) => setForm(form => ({...form, period:e.target.value}))}/>
                </FormGroup>
                <FormGroup block column>
                    <fieldset>
                        <legend>Description</legend>
                        <textarea placeholder="Descrição" rows={5} value={form.description}
                                  onChange={(e) => setForm(form => ({...form, description:e.target.value}))}/>
                    </fieldset>
                </FormGroup>
                <InlineForm>
                    <SubmitBtn disabled={loading} onClick={handleSubmit}>Salvar</SubmitBtn>
                    <ResetBtn disabled={loading} onClick={handleReset}>Cancelar</ResetBtn>
                </InlineForm>
            </Modal>
        </Container>
    );
}

export default CreationModal;