import styled from "styled-components";
import {FormGroup, InlineForm} from "../Inputs";

export const Modal = styled.div`
  position: relative;
  background: #fff;
  border: 1px solid #e5e5e5;
  border-radius: 8px;
  box-shadow: 3px 3px 10px 5px rgba(0,0,0,0.4);
  width: 90%;
  height: 48%;
  padding: 10px 20px;
  
  ${FormGroup} {
    margin: 10px 0;
  }
  
  ${InlineForm} {
    position: absolute;
    bottom: 20px;
    right: 20px
  }
`;

export const Container = styled.div`
  background: rgba(255,255,255,0.8);
  position: absolute;
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100%;
  width: 100%;
  z-index: 1;
`;