import styled from "styled-components";

export const Content = styled.div`
  overflow: hidden;
  position: relative;
  padding: 0 15px;
  top: 0;
  right: 0;
  
  .actions-container {
    display: flex;
    justify-content: flex-end;
    margin-bottom: 10px;
    input[type="submit"]:first-child, input[type="reset"]:first-child {
      margin-right: 20px;
    } 
  }
  
  input[type="submit"], input[type="reset"] {
    all: unset;
    height: 38px;
    text-align: center;
    font-size: 18px;
    border-radius: 5px;
    padding: 0 20px;
    width: auto;
    cursor: pointer;
    transition: 0.2s, 0.1s transform;

    :active {
      transform: scale(0.95);
    }
  }

  input[type="submit"] {
    color: mediumslateblue;
    :hover {
      color: #fff;
      background: mediumslateblue;
    }
  }

  input[type="reset"] {
    color: grey;
    :hover {
      color: #fff;
      background: grey;
    }
  }
`;

export const Container = styled.div`
  display: flex;
  align-items: flex-start;
  position: relative;
  min-height: 40px;
  max-height: 40px;
  width: 100%;
  background: #fff;
  border: 1px solid #ececec;
  border-radius: 5px;
  padding-left: 15px;
  overflow: hidden;
  transition: 0.2s;
  
  button {
    svg {
      pointer-events: none;
      transition: 0.2s;
    }
  }
  
  button:first-child {
    position: absolute;
    top: 9px;
    left: 0px;
  }
  
  button:last-child {
    position: absolute;
    top: 9px;
    right: 10px;
  }
  
  & > * {
    margin-left: 10px;
  }

  input, textarea {
    outline: none;
    box-shadow: 0px 0px 5px 1px cornflowerblue;
    border: 1px solid cornflowerblue;
    border-radius: 5px;
    margin: 5px 0;
    width: 100%;
    font-size: 16px;
    font-weight: 500;
    color: #555555;
    transition: 0.2s;
    
    :disabled {
      opacity: 1;
      background: transparent;
      box-shadow: none;
      border: none;
      margin: 0;
      cursor: default;
    }
  }
  
  input {
    padding: 10px;
    height: 38px;
    text-decoration: ${props => props.checked ? "line-through" : "none"};
  }
  
  textarea {
    font: unset;
    resize: none;
    font-size: 15px;
    font-weight: 500;
    color: #555555;
    padding: 10px;
  }
  
  &.opened {
    max-height: 300px;
    
    button:first-child svg {
      transform: rotate(90deg);
    }
    
    ${Content} {
      padding-top: 10px;
    }
  }
  &.slided {
    transform: translateX(-150px);
  }
`;