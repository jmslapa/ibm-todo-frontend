import {Container, Content} from "./style";
import {ChevronRight, DotsHorizontalRounded, Exit} from 'styled-icons/boxicons-regular'
import {useCallback, useEffect, useRef, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {updateTask} from "../../store/modules/list/actions";
import * as yup from "yup";
import {toast} from "react-toastify";

const Task = ({ task, editing, setEditing }) => {

    const dispatch = useDispatch();
    const initialForm = task;
    const loading = useSelector(state => state.common.loading);
    const [form, setForm] = useState(initialForm);
    const [opened, setOpened] = useState(false);
    const [slided, setSlided] = useState(false);
    const container = useRef(null);

    const todoSchema = yup.object().shape({
        title: yup.string().min(3).max(100).required(),
        // period: yup.date().min(today).required(),
        description: yup.string().min(20).max(500),
        complete: yup.boolean().required()
    });

    const toggleCollapse = useCallback(() => {
        setOpened(opened => !opened);
    }, []);

    const toggleSlide = useCallback(() => {
        setSlided(slided => !slided);
    }, []);

    const onTaskChange = useCallback(() => {
        setSlided(false);
        setOpened(false);
    }, [])

    function resolveClassName() {
        if(opened) {
            return "opened"
        } else if (slided) {
            return "slided"
        } else {
            return "";
        }
    }

    function handleSubmit(e) {
        e.preventDefault();
        todoSchema.validate(form)
            .then(() => {
                dispatch(updateTask(form));
                setOpened(false);
                setEditing(false);
            })
            .catch(err => {
                toast.error(err.message.charAt(0).toUpperCase() + err.message.slice(1));
                setForm(initialForm);
            });
    }

    function handleReset(e) {
        e.preventDefault();
        setForm(initialForm);
        setOpened(false);
        setEditing(false);
    }

    useEffect(() => {
        if(editing) {
            setOpened(true);
            setSlided(false);
        }
    }, [editing])

    useEffect(() => {
        onTaskChange();
    }, [ task, onTaskChange ]);

    return (
        <Container ref={container} checked={task.complete} className={resolveClassName()}>
            <button type="button" onClick={toggleCollapse}>
                <ChevronRight size={20}/>
            </button>
            <Content>
                <form onSubmit={handleSubmit} onReset={handleReset}>
                    <input type="text" disabled={!editing} value={form.title}
                           onChange={(e) => setForm(state => ({...state, title: e.target.value}))}/>
                    <textarea rows="5" maxLength="500" minLength="20" disabled={!editing} value={form.description}
                              onChange={(e) => setForm(state => ({...state, description: e.target.value}))}/>
                    {
                        editing
                            ? <div className="actions-container">
                                <input type="submit" value="Salvar" disabled={loading}/>
                                <input type="reset" value="Cancelar" disabled={loading}/>
                            </div>
                            : false
                    }
                </form>
            </Content>
            {
                !opened && !editing
                    ? <button type="button" onClick={toggleSlide}>
                        {!slided ? <DotsHorizontalRounded size={20}/> : <Exit size={20}/>}
                    </button>
                    : false
            }
        </Container>
    );
}

export default Task;