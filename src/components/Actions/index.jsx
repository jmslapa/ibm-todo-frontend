import {CheckBtn, Container, DeleteBtn, EditBtn, UncheckBtn} from "./style";
import { Edit, TrashAlt, CheckCircle, XCircle } from 'styled-icons/boxicons-solid'


const Actions = ({ onEdit, onDelete, onCheck, checked }) => {
    return (
        <Container>
            <EditBtn type="button" onClick={onEdit}>
                <Edit/>
            </EditBtn>
            <DeleteBtn type="button" onClick={onDelete}>
                <TrashAlt/>
            </DeleteBtn>
            {
                !checked
                ? <CheckBtn type="button" onClick={onCheck}>
                        <CheckCircle/>
                    </CheckBtn>
                : <UncheckBtn type="button" onClick={onCheck}>
                        <XCircle/>
                    </UncheckBtn>
            }

        </Container>
    );
}

export default Actions;