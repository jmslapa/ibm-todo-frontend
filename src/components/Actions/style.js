import styled from "styled-components";

export const Container = styled.div`
  display: inline-flex;
  position: absolute;
  top: 0;
  right: 10px;
  height: 100%;
  
  button {
    margin: 5px;
    height: 30px;
    width: 30px;
    transition: 0.2s, 0.1s transform;
    
    &:active {
      transform: scale(0.9);
    }    
  }
  
  button svg {
    pointer-events: none;
    width: 20px;
  }
`;

export const EditBtn = styled.button`
  border-radius: 4px;
  border: 2px solid mediumslateblue;
  color: mediumslateblue;

  &:hover {
    color: #fff;
    background: mediumslateblue;
  }
`;
export const DeleteBtn = styled.button`
  border-radius: 4px;
  border: 2px solid red;
  color: red;

  &:hover {
    color: #fff;
    background: red;
  }
`;

export const CheckBtn = styled.button`
  border-radius: 4px;
  border: 2px solid green;
  color: green;

  &:hover {
    color: #fff;
    background: green;
  }
`;

export const UncheckBtn = styled.button`
  border-radius: 4px;
  border: 2px solid gold;
  color: gold;

  &:hover {
    color: #fff;
    background: gold;
  }
`;
