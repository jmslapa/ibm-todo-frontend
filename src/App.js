import React from 'react';
import {ToastContainer} from 'react-toastify';
import {PersistGate} from 'redux-persist/integration/react';
import {Provider} from 'react-redux';
import {store, persistor} from './store';
import GlobalStyle from './styles/global';
import Layout from "./components/Layout";

function App() {
    return (
        <Provider store={store}>
            <PersistGate persistor={persistor}>
                <GlobalStyle/>
                <ToastContainer autoClose={3000}/>
                <Layout/>
            </PersistGate>
        </Provider>
    );
}

export default App;